#include <Game/Game.h>

#include <graphics/GraphicsEngine.h>
#include <graphics/Window.h>

#include <Windows.h>
#include <iostream>

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
static bool s_GameRunning = true; //we'll need this in the WindowProc

int WINAPI WinMain(HINSTANCE instance, HINSTANCE, LPSTR, int)
{

	Window window{ { 1600, 900, instance, WindowProc } };

	Graphics::GraphicsEngine graphics_engine;

	graphics_engine.CreateDeviceAndSwapchain(window);

	window.SetText("Headless");

	Game game;
	MSG msg;
	do
	{
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT || msg.message == WM_CLOSE || game.HasQuit())
		{
			s_GameRunning = false;
			break;
		}

		graphics_engine.Present();

		char temp[100];
		sprintf_s(temp, "%.3f", game.GetFPS());
		window.SetText(temp);

	} while (s_GameRunning);

	game.OnExit();

	return 0;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
		case WM_CREATE:
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		case WM_SIZE:
			break;
		case WM_ACTIVATE:
		{
			if (LOWORD(wParam) == WA_INACTIVE)
			{
			}
			else
			{
			}
		}break;
		case WM_ENTERSIZEMOVE:
			break;
		case WM_EXITSIZEMOVE:
			break;
		case WM_CLOSE:
			s_GameRunning = false;
			break;
		case WM_SYSCOMMAND:
			if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
				return 0;
			break;
		case WM_SYSKEYDOWN:
			if (wParam == VK_RETURN)
			{
				if ((HIWORD(lParam) & KF_ALTDOWN))
				{

				}
			}
			break;
		case WM_INPUT:
		{
		} break;
		default:
			break;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}