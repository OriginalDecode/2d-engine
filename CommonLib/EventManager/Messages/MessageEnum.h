#pragma once

enum class eMessageType
{
	ON_LEFT_CLICK,
	COLLIDED,
	COUNT,
};