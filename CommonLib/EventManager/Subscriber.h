#pragma once

#include "../Types.hpp"
class Subscriber
{
public:
	Subscriber() = default;

	virtual void HandleEvent(uint64 /*event*/, void* /*pData*/) { };
	//virtual void ReceiveMessage(const Event& ) { }

};
