#include "Threadpool.h"
#include <thread>
#include <sstream>

void Threadpool::Initiate(const std::string& debug_name)
{
	const int thread_count = std::thread::hardware_concurrency();
	m_Workers.Init(thread_count);

	for (int i = 0; i < thread_count; i++)
	{
		std::stringstream ss;
		ss << debug_name << i;
		m_Workers.Add(Worker(ss.str()));
		
	}
	m_Workers.Optimize();
}

void Threadpool::Update()
{
	for(Worker& worker : m_Workers)
	{
		if (!HasWork())
			break;

		if (worker.IsDone())
		{
			worker.AddWork(m_Work.front());
			m_Work.pop();
		}
	}
}

void Threadpool::AddWork(Work aWorkOrder)
{
	m_Work.push(aWorkOrder);
}

bool Threadpool::CurrentWorkFinished() const
{
	for (const Worker& worker : m_Workers)
	{
		if (!worker.IsDone())
			return false;
	}
	return true;
}
