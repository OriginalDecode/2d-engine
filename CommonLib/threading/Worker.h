#pragma once
#include <atomic>
#include "Work.h"
namespace std
{
	class thread;
}

class Worker
{
public:
	Worker() = default;
	Worker(const std::string& debug_name);
	~Worker();


	bool IsDone() const;
	void AddWork(const Work& work);
	void Stop();

private:
	std::thread* m_WorkThread;
	Work m_Work;
	volatile std::atomic<bool> m_IsDone = true;
	volatile std::atomic<bool> m_IsRunning = true;
	void Run();

};

