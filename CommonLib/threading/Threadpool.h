#pragma once
#include <queue>
#include "../containers/GrowingArray.h"
#include "Worker.h"
#include "Work.h"
class Threadpool
{
public:
	Threadpool() = default;

	void Initiate(const std::string& debug_name);
	void Update();
	void AddWork(Work aWorkOrder);
	
	bool HasWork() { return !m_Work.empty(); }
	size_t GetWorkSize() const { return m_Work.size(); }
	bool CurrentWorkFinished() const;
private:
	std::queue<Work> m_Work;
	CU::GrowingArray<Worker> m_Workers;
};

