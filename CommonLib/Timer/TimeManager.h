#pragma once

#include <vector>
#include "Timer.h"
#include "Time.h"

namespace CommonUtilities
{
	class TimeManager
	{
	public:
		
		TimeManager();
		~TimeManager() = default;
		void Update();

		void UpdateMaster();

		int CreateTimer();

		Base::Timer& GetTimer(int anIndex);
		inline float GetDeltaTime() const;
		inline float GetFPS() const;
		inline float GetFrameTime() const;

		void Pause();
		void Start();

		Base::Timer& GetMasterTimer() { return myMasterTimer; }

	private:
		//static TimeManager* myInstance;

		std::vector<Base::Timer> myTimers;
		Base::Timer myMasterTimer;
	};

	float TimeManager::GetDeltaTime() const
	{
		myMasterTimer.GetTime();
		return myMasterTimer.GetTime().GetSeconds();
	}

	float TimeManager::GetFPS() const 
	{
		return static_cast<float>(1) / GetDeltaTime();
	}

	float TimeManager::GetFrameTime() const
	{
		return myMasterTimer.GetTime().GetSeconds();
	}


}
