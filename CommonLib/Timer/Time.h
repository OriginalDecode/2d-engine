#pragma once
#include <Windows.h>

namespace Base
{
	class Time
	{
	public:
		Time() = default;
		~Time() = default;

		void Update(LARGE_INTEGER& aLastEnd, LARGE_INTEGER& anEnd, LARGE_INTEGER& aClockFreq);

		float GetHours() const;
		float GetMinutes() const;
		float GetSeconds() const;
		float GetMilliseconds() const;
		float GetMicroseconds() const;
		float GetFPS() const;

		void SetTime(float someTime);

	private:
		float m_Time = 0.f;

	};
}



