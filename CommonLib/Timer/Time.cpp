#include "Time.h"

namespace Base
{
		void Time::Update(LARGE_INTEGER& aLastEnd, LARGE_INTEGER& anEnd, LARGE_INTEGER& aClockFreq)
		{
			m_Time = (anEnd.QuadPart - aLastEnd.QuadPart) / static_cast<float>(aClockFreq.QuadPart);
		}

		float Time::GetHours() const
		{
			return (m_Time / 60.f) / 60.f;
		}

		float Time::GetMinutes() const
		{
			return (m_Time / 60.f);
		}

		float Time::GetSeconds() const
		{
			return m_Time;
		}

		float Time::GetMilliseconds() const
		{
			return m_Time * 1000.f;
		}

		float Time::GetMicroseconds() const
		{
			return m_Time / 1'000'000.f;
		}

		float Time::GetFPS() const
		{
			return 1 / m_Time;
		}

		void Time::SetTime(float someTime)
		{
			m_Time = someTime;
		}
}