#pragma once
#include "Time.h"
namespace Base
{
	class Timer
	{
	public:
		Timer();
		~Timer() = default;

		void Update();

		void Start();
		void Stop();
		void Pause();
		void Resume();

		const Time& GetTime() const;
		const Time& GetTotalTime() const;


	private:

		Time m_CurrentTime;
		Time m_TotalTime;

		LARGE_INTEGER m_Frequency;
		LARGE_INTEGER m_Start; // application start time

		LARGE_INTEGER m_Current; // frame start time
		LARGE_INTEGER m_Prev; // prev frame start time

		bool m_IsActive : 4;
		bool m_IsPaused : 4;
	};
}
