#include "Timer.h"

#ifdef _WIN32
#include <Windows.h>
#endif

#include <cassert>

namespace Base
{
	Timer::Timer()
		: m_IsPaused(true)
		, m_IsActive(false)
	{
		QueryPerformanceFrequency(&m_Frequency);
	}
	
	void Timer::Update()
	{
		if(!m_IsActive || m_IsPaused)
			return;

		if (!QueryPerformanceCounter(&m_Current))
			assert(!"Failed to query counter!");

		m_TotalTime.Update(m_Start, m_Current, m_Frequency);
		m_CurrentTime.Update(m_Prev, m_Current, m_Frequency);
		m_Prev = m_Current;
	}

	void Timer::Start()
	{
		if(!m_IsPaused && !m_IsActive)
		{
			assert(!"Cannot start a timer that was not stopped! Did you mean Resume()?");
			return;
		}

		m_IsActive = true;
		m_IsPaused = false;
		if (!QueryPerformanceCounter(&m_Start))
			assert(!"Failed to query counter!");
		m_Prev = m_Start;
		m_Current = m_Start;
	}

	void Timer::Stop()
	{
		m_IsActive = false;
		m_IsPaused = false;
	}

	void Timer::Pause()
	{
		m_IsActive = false;
		m_IsPaused = true;
	}

	void Timer::Resume()
	{
		if(!m_IsPaused && !m_IsActive)
		{
			assert(!"Cannot resume timer that was not paused! Did you mean Start()?");
			return;
		}

		m_IsActive = true;
		m_IsPaused = false;
		m_Prev = m_Current;
		if (!QueryPerformanceCounter(&m_Current))
			assert(!"Failed to query counter!");

		m_Start.QuadPart += (m_Current.QuadPart - m_Prev.QuadPart);
	}

	const Time& Timer::GetTime() const
	{
		return m_CurrentTime;
	}

	const Time& Timer::GetTotalTime() const
	{
		return m_TotalTime;
	}

}