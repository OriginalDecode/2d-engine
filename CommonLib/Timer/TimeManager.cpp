#include "TimeManager.h"
#include <assert.h>



namespace CommonUtilities
{
	TimeManager::TimeManager()
	{
		myMasterTimer.Start();
	}

	int TimeManager::CreateTimer()
	{
		Base::Timer timer;
		timer.Start();
		myTimers.push_back(timer);
		return static_cast<int>(myTimers.size()) - 1;
	}

	void TimeManager::Update()
	{
		myMasterTimer.Update();
		for (Base::Timer& timer : myTimers)
		{
			timer.Update();
		}
	}

	void TimeManager::UpdateMaster()
	{
		myMasterTimer.Update();
	}

	Base::Timer& TimeManager::GetTimer(int anIndex)
	{
		return myTimers[anIndex];
	}

	void TimeManager::Pause()
	{
		myMasterTimer.Pause();
	}

	void TimeManager::Start()
	{
		myMasterTimer.Start();
	}

}
