#include "Utilities.h"
#include <comdef.h>
#include <cassert>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <vadefs.h>
#include <stdarg.h>
#include <thread>
#ifdef _WIN32
#include <Windows.h>
#endif

#include "Hash.h"


namespace cl
{
	bool file_exist(std::string path)
	{
		std::ifstream stream(path.c_str());
		return stream.good();
	}

	const int64 nearest_pow(int64 value)
	{
		value--;
		value |= value >> 1;
		value |= value >> 2;
		value |= value >> 4;
		value |= value >> 8;
		value |= value >> 16;
		value++;
		return value;
	}

	void ToLower(std::string& str)
	{
		std::transform(str.begin(), str.end(), str.begin(), tolower);
	}

	float ClampF(float value, float min, float max)
	{
		if (value > max)
			return max;

		if (value < min)
			return min;

		return value;
	}

	int ClampI(int value, int min, int max)
	{
		if (value > max)
			return max;

		if (value < min)
			return min;

		return value;
	}

	float RadToDegree(float rad)
	{
		return rad * (180.f / 3.1415926535f);
	}

	float DegreeToRad(float deg)
	{
		return deg * (3.1415926535f / 180.f);
	}

	constexpr float _DegToRad(const float x)
	{
		return x * (3.14f / 180.f);
	}

	CU::Vector2f Bezier(CU::Vector2f p1, CU::Vector2f p2, CU::Vector2f p3, float t)
	{
		float xa = CalcPointOnCurve(p1.x, p2.x, t);
		float ya = CalcPointOnCurve(p1.y, p2.y, t);

		float xb = CalcPointOnCurve(p2.x, p3.x, t);
		float yb = CalcPointOnCurve(p2.y, p3.y, t);

		CU::Vector2f toReturn;
		toReturn.x = CalcPointOnCurve(xa, xb, t);
		toReturn.y = CalcPointOnCurve(ya, yb, t);
		return toReturn;
	}

	CU::Vector3f Bezier(CU::Vector3f p1, CU::Vector3f p2, CU::Vector3f p3, float t)
	{
		float u = 1.f - t;

		float tt = t * t;

		float uu = u * u;

		float uuu = uu * u;

		float ttt = tt * t;

		CU::Vector3f toReturn = p1 * uuu;
		toReturn += p2 * (3 * uu * t);
		toReturn += p3 * (3 * u * tt);
		toReturn += p3 * ttt;
		return toReturn;
	}



	CU::Vector3f CubicBezier(CU::Vector3f p1, CU::Vector3f p2, CU::Vector3f p3, CU::Vector3f p4, float t)
	{
		float u = 1.f - t;

		float tt = t * t;

		float uu = u * u;

		float uuu = u * u * u;

		float ttt = t* t * t;

		CU::Vector3f toReturn = p1 * uuu;
		toReturn += p2 * (3 * uu * t);
		toReturn += p3 * (3 * u * tt);
		toReturn += p4 * ttt;
		return toReturn;
	}

	float CalcPointOnCurve(float x, float y, float t)
	{
		float diff = y - x;
		return x + (diff * t);
	}


	unsigned int binomialCoef(int n, int k)
	{
		int r = 1;
		if (k > n)
			return 0;

		for (int d = 1; d <= k; d++)
		{
			r *= n--;
			r /= d;
		}
		return r;
	}
};

uint64 Hash(std::string key)
{
	std::transform(key.begin(), key.end(), key.begin(), tolower);

	uint64 result;
	MurmurHash3_x86_32(key.c_str(), static_cast<int>(key.length()), 0, &result);
	return result;

}

#ifdef _WIN32
#pragma region ThreadNaming
const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)
typedef struct tagTHREADNAME_INFO
{
	DWORD dwType; // Must be 0x1000.
	LPCSTR szName; // Pointer to name (in user addr space).
	DWORD dwThreadID; // Thread ID (-1=caller thread).
	DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)
void SetThreadName(DWORD dwThreadID, const char* threadName)
{
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = threadName;
	info.dwThreadID = dwThreadID;
	info.dwFlags = 0;
#pragma warning(push)
#pragma warning(disable: 6320 6322)
	__try {
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) {
	}
#pragma warning(pop)
}
#endif


namespace cl
{
	void SetThreadName(const std::thread::id& id, const char* name)
	{

		std::stringstream ss;
		ss << id;
		DWORD word_id;
		ss >> word_id;
		::SetThreadName(word_id, name);
	}

	std::string HandleVAArgs(const char* formatted_string, ...)
	{
		char buffer[SHRT_MAX];
		va_list args;
		va_start(args, formatted_string);
		vsprintf_s(buffer, formatted_string, args);
		va_end(args);
		return buffer;
	}

	const std::wstring ToWideStr(const std::string& str)
	{
		 return std::wstring(str.begin(), str.end()); 
	}

	std::string GuidToString(const GUID& guid)
	{
		wchar_t* wstr = nullptr;
		HRESULT hr = StringFromCLSID(guid, &wstr);
		assert(hr == S_OK && "Failed to convert to string!");
		_bstr_t converted(wstr);
		::CoTaskMemFree(wstr);
		return std::string(converted);
	}

	GUID StrToGuid(const std::string& str)
	{
		GUID out;
		HRESULT hr = CLSIDFromString(ToWideStr(str).c_str(), &out);
		assert(hr == S_OK && "Failed to convert string to GUID!");
		return out;
	}

	std::vector<File> FindFilesInDirectory(const char* directory_path)
	{
		std::vector<File> files;
#ifdef _WIN32

		
		WIN32_FIND_DATA fd;
		HANDLE hFind = ::FindFirstFile(directory_path, &fd);
		if (hFind != INVALID_HANDLE_VALUE) {
			do 
			{
				File file;
				ZeroMemory(&file, 260 * sizeof(char));

				if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) 
				{
					memcpy(&file.filename, fd.cFileName, 260 * sizeof(char));
				}
				if(strlen(file.filename) > 0)
					files.push_back(file);

			} while (::FindNextFile(hFind, &fd));
			::FindClose(hFind);
		}
#endif
		return files;
	}
}