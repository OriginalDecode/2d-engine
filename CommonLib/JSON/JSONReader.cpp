#include "JSONReader.h"
#include <assert.h>
#include <DL_Debug/DL_Debug.h>

namespace JsonError
{
	const char* Error = "JSON_NO_STRING_FOUND";

};
JSONReader::~JSONReader()
{
	CloseDocument();
}

void JSONReader::ReadFromFile(const char* filepath)
{
	m_Filepath = filepath;
	
	fopen_s(&m_File, m_Filepath, "r");
	if (m_File)
	{
		char buffer[2048]; //the buffer size determines how fast it can parse the file
		m_Stream = new rapidjson::FileReadStream(m_File, buffer, sizeof(buffer));
		m_Doc.ParseStream<0, rapidjson::UTF8<>, rapidjson::FileReadStream>(*m_Stream);
	}
	else
	{
		assert(m_File != NULL && "File could not be found!");
	}
}

void JSONReader::ReadFromMemory(const char* buffer)
{
	m_Doc.Parse<rapidjson::kParseStopWhenDoneFlag>(buffer);
	assert(!m_Doc.HasParseError() && "An error happened while parsing json.");
}

void JSONReader::CloseDocument()
{

	delete m_Stream;
	m_Stream = nullptr;

	fclose(m_File);
	m_File = nullptr;

}

bool JSONReader::DocumentHasMember(const char* tag)
{
	assert(m_File && "json file not open.");
	assert(m_Stream && "JSONReader not initiated. Reader was null!");
	assert(m_Doc != 0 && "Document had no valid FileReader attatched!");

	return m_Doc.HasMember(tag);
}

bool JSONReader::ElementHasMember(const rapidjson::Value& element, const char* tag)
{
	assert(m_File && "json file not open.");
	assert(m_Stream && "JSONReader not initiated. Reader was null!");
	assert(m_Doc != 0 && "Document had no valid FileReader attatched!");

	return element.HasMember(tag);
}

void JSONReader::ReadElement(const char* tag, bool& out)
{
	if (DocumentHasMember(tag))
		out = m_Doc[tag].GetBool();
}

void JSONReader::ReadElement(const char* tag, int& out)
{
	if(DocumentHasMember(tag))
		out = m_Doc[tag].GetInt();
}

void JSONReader::ReadElement(const char* tag, unsigned int& out)
{
	if (DocumentHasMember(tag))
		out = m_Doc[tag].GetUint();
}

void JSONReader::ReadElement(const char* tag, float& out)
{
	if (DocumentHasMember(tag))
		out = (float)m_Doc[tag].GetDouble();
}

void JSONReader::ReadElement(const char* tag, double& out)
{
	if (DocumentHasMember(tag))
		out = m_Doc[tag].GetDouble();
}

void JSONReader::ReadElement(const char* tag, std::string& out)
{
	out = JsonError::Error;
	if (DocumentHasMember(tag))
		out = m_Doc[tag].GetString();
}

std::string JSONReader::ReadElement(const char* tag)
{
	std::string return_value;
	ReadElement(tag, return_value);
	return return_value;
}

void JSONReader::ReadElement(const rapidjson::Value& element, CU::Vector3f& out)
{
	out.x = (float)element[0].GetDouble();
	out.y = (float)element[1].GetDouble();
	out.z = (float)element[2].GetDouble();
}

void JSONReader::ReadElement(const rapidjson::Value& element, CU::Vector4f& out)
{
	out.x = (float)element[0].GetDouble();
	out.y = (float)element[1].GetDouble();
	out.z = (float)element[2].GetDouble();
	out.w = (float)element[3].GetDouble();
}

void JSONReader::ReadElement(const rapidjson::Value& element, float& out)
{
	out = (float)element.GetDouble();
}

void JSONReader::ReadElement(const JSONElement& el, const char* tag, bool& out)
{
	if (ElementHasMember(el, tag))
		out = el[tag].GetBool();
}

void JSONReader::ReadElement(const JSONElement& el, const char* tag, int& out)
{
	if (ElementHasMember(el, tag))
		out = el[tag].GetInt();

}

void JSONReader::ReadElement(const JSONElement& el, const char* tag, unsigned int& out)
{
	if (ElementHasMember(el, tag))
		out = el[tag].GetUint();

}

void JSONReader::ReadElement(const JSONElement& el, const char* tag, float& out)
{
	if (ElementHasMember(el, tag))
		out = (float)el[tag].GetDouble();

}

void JSONReader::ReadElement(const JSONElement& el, const char* tag, double& out)
{
	if (ElementHasMember(el, tag))
		out = el[tag].GetDouble();

}

void JSONReader::ReadElement(const JSONElement& el, const char* tag, std::string& out)
{
	if (ElementHasMember(el, tag))
		out = el[tag].GetString();

}

std::string JSONReader::ReadElement(const JSONElement& el, const char* tag)
{
	assert(el.HasMember(tag) && "Failed to find tag!");
	return el[tag].GetString();
}

std::string JSONReader::OptionalReadElement(const JSONElement& el, const char* tag)
{
	if(el.HasMember(tag))
		return el[tag].GetString();

	return std::string();
}



const JSONElement& JSONReader::GetElement(const char* element_name)
{
	assert(DocumentHasMember(element_name) && "failed to find element!");
	return m_Doc[element_name];
}

