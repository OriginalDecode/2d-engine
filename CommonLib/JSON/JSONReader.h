#pragma once
#include "include\document.h"
#include "include\filereadstream.h"
#include "..\Math\Vector\Vector.h"
typedef int FRESULT;
typedef rapidjson::Value  JSONElement;
class JSONReader
{
public:
	JSONReader() = default;
	~JSONReader();

	const JSONElement& GetElement(const char* element);

	bool DocumentHasMember(const char* tag);
	bool ElementHasMember(const rapidjson::Value& element, const char* tag);

	void ReadElement(const char* tag, bool& out);
	void ReadElement(const char* tag, int& out);
	void ReadElement(const char* tag, unsigned int& out);
	void ReadElement(const char* tag, float& out);
	void ReadElement(const char* tag, double& out);
	void ReadElement(const char* tag, std::string& out);
	
	void ReadElement(const JSONElement& el, const char* tag, bool& out);
	void ReadElement(const JSONElement& el, const char* tag, int& out);
	void ReadElement(const JSONElement& el, const char* tag, unsigned int& out);
	void ReadElement(const JSONElement& el, const char* tag, float& out);
	void ReadElement(const JSONElement& el, const char* tag, double& out);
	void ReadElement(const JSONElement& el, const char* tag, std::string& out);

	
	
	std::string ReadElement(const char* tag);
	std::string ReadElement(const JSONElement& el, const char* tag);

	
	void ReadElement(const rapidjson::Value& element, CU::Vector3f& out);
	void ReadElement(const rapidjson::Value& element, CU::Vector4f& out);
	void ReadElement(const rapidjson::Value& element, float& out);


	void ReadFromFile(const char* filepath);
	void ReadFromMemory(const char* buffer);
	void CloseDocument();

	std::string OptionalReadElement(const JSONElement& el, const char* tag);
	const rapidjson::Document& GetDocument() const { return m_Doc; }

private:

	rapidjson::Document m_Doc;
	rapidjson::FileReadStream* m_Stream;


	const char* m_Filepath;
	FILE* m_File = nullptr;

};
