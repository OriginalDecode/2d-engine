#pragma once
#include <cassert>

namespace CommonUtilities
{
	template<typename Type, int T2 = 32>
	class StaticArray
	{
	public:
		StaticArray();
		StaticArray(const StaticArray& aStaticArray);

		~StaticArray();

		StaticArray& operator=(const StaticArray& aStaticArray);

		const Type& operator[](const int& aIndex) const;
		Type& operator[](const int& aIndex);


		// Utility functions
		void Insert(int aIndex, Type& aObject);
		void DeleteAll();
		void InsertLast(Type& object);

		typedef Type* iterator;
		typedef const Type* const_iterator;
		iterator begin() { return &m_Data[0]; }
		const_iterator begin() const { return &m_Data[0]; }
		iterator end() { return &m_Data[T2]; }
		const_iterator end() const { return &m_Data[T2]; }

		bool operator==(const StaticArray<Type>& other);

		int Capacity() { return m_Capacity; }

	private:
		Type m_Data[T2];
		int m_Capacity = 0;
		int m_LastIndex = 0;
	};

	template<typename Type, int T2>
	bool StaticArray<Type, T2>::operator==(const StaticArray<Type>& other)
	{

		for (int i = 0; i < T2; i++)
		{
			if ( m_Data[i] != other[i] )
				return false;
		}

		return true;
	}

	template<typename Type, int T2 = 32>
	StaticArray<Type, T2>::StaticArray()
	{
		m_Capacity = sizeof(m_Data) / sizeof(int);
	}

	template<typename Type, int T2 = 32>
	StaticArray<Type, T2>::~StaticArray()
	{

	}

	template<typename Type, int T2 = 32>
	StaticArray<Type, T2>::StaticArray(const StaticArray& aStaticArray)
	{
		operator=(aStaticArray);
	}

	template<typename Type, int T2 = 32>
	StaticArray<Type, T2>& StaticArray<Type, T2>::operator=(const StaticArray& aStaticArray)
	{
		for (int i = 0; i < T2; ++i)
		{
			m_Data[i] = aStaticArray.m_Data[i];
		}
		return *this;
	}

	template<typename Type, int T2 = 32>
	const Type& StaticArray<Type, T2>::operator[](const int& aIndex) const
	{
		assert(aIndex >= 0 && "Index has to be 0 or more.");
		assert(aIndex < T2 && "a index out of bounds!");
		return m_Data[aIndex];
	}

	template<typename Type, int T2 = 32>
	Type& StaticArray<Type, T2>::operator[](const int& aIndex)
	{
		assert(aIndex >= 0 && "Index has to be 0 or more.");
		assert(aIndex < T2 && "a index out of bounds!");
		return m_Data[aIndex];
	}

	template<typename Type, int T2 = 32>
	void StaticArray<Type, T2>::Insert(int aIndex, Type& aObject)
	{
		assert(aIndex >= 0 && "Index has to be 0 or more.");
		assert(aIndex < T2 && "a index out of bounds!");
		for (int i = T2 - 2; i >= aIndex; --i)
		{
			m_Data[i + 1] = m_Data[i];
		}
		m_Data[aIndex] = aObject;
	}

	template<typename Type, int T2 = 32>
	void StaticArray<Type, T2>::InsertLast(Type& object)
	{
		assert(m_LastIndex < m_Capacity && "Can't add to last if container is full.");
		m_Data[m_LastIndex] = object;
		m_LastIndex++;
	}

	template<typename Type, int T2 = 32>
	void StaticArray<Type, T2>::DeleteAll()
	{
		for (int i = 0; i < T2; ++i)
		{
			delete m_Data[i];
			m_Data[i] = nullptr;
		}
	}

};
namespace CU = CommonUtilities;