#include "Game.h"
#include <thread>

#include "CommonLib/Utilities/Utilities.h"

Game::Game()
{
	m_Timer.Start();
	m_States.PushState(&m_MainState, StateStack::MAIN);
	m_Thread = new std::thread([&] {  Game::Update();  });
}

Game::~Game()
{
}

void Game::OnExit()
{
	m_HasQuit = true;
	m_Thread->join();
	delete m_Thread;
	m_Thread = nullptr;
}

float Game::GetFPS() const
{
	return m_Timer.GetTime().GetFPS();
}

void Game::Update()
{
	while (!m_HasQuit)
	{
		m_Timer.Update();
		if (!m_States.UpdateCurrentState(m_Timer.GetTime().GetMilliseconds()))
		{
			//m_Synchronizer->LogicIsDone();
			//m_Synchronizer->WaitForRender();
			//break;
		}

	}
}
