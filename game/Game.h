#pragma once
#include "StateStack.h"
#include "MainMenuState.h"
#include "CommonLib/Timer/Timer.h"

namespace std
{
	class thread;
}

class Game
{
public:
	Game();
	~Game();


	bool HasQuit() const { return m_HasQuit; }

	void OnExit();

	float GetFPS() const;

private:
	void Update();

	bool m_HasQuit = false;
	std::thread* m_Thread = nullptr;
	StateStack m_States;
	MainMenuState m_MainState;
	Base::Timer m_Timer;
};

