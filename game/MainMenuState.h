#pragma once
#include "State.h"
class MainMenuState : public State
{
public:
	MainMenuState();
	~MainMenuState();

	void InitState(StateStack* statestack) override;
	void EndState() override;
	void Render(bool render_through) override;
	void Update(float aDeltaTime) override;


};

