#pragma once

#include <CommonLib/containers/GrowingArray.h>
#include <CommonLib/Types.hpp>

class State;
typedef CU::GrowingArray<State*> SubStateContainer;
typedef CU::GrowingArray<SubStateContainer> MainStateContainer;
class StateStack
{
public:
	StateStack();

	enum eGameState
	{
		MAIN,
		SUB
	};
	void PopCurrentMainState();
	void PopCurrentSubState();
	void PushState(State* game_state, eGameState game_state_type);
	void PauseCurrentState();
	void ResumeCurrentState();
	//void PopState(u32 state_id);

	bool UpdateCurrentState(float dt);


	void Clear();

private:
	MainStateContainer m_GameStates;
	int32 m_MainIndex = 0;
	int32 m_SubIndex = 0;
};

